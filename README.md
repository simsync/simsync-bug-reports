# How to create an issue or bug report (Discord or Application)

#### 1. Navigate to the issue UI

You can find the issue UI here: https://gitlab.com/simsync/simsync-bug-reports/issues. You will have to sign in or create an account, which takes 5 seconds. 

Then just click the `New Issue` button. Remember there is an option for confidentiality right below your message. It should look like this:

<img src="https://i.gyazo.com/821fb560f7755860576c510df3a29730.png"  width="700" height="43">

#### 2. Or you can send an email here:
`incoming+simsync-simsync-bug-reports-16391709-issue-@incoming.gitlab.com`

These are automatically confidential.


### What does a proper bug or issue report look like?

We cannot give you the exact formula for what is wrong. However, we just ask that you give as much information as possible. 

##### Want us to help you?
If you're requesting us to help you with something, include your **Discord Name and Number** (Example: Sheepii#0001), or maybe an email. Just be aware if you're posting personal information, click the confidential button above and only the developers will see it. 

##### Further help?
If you have any further questions, don't hestitate to ask on our [Discord](https://discord.gg/g22dU5a) in the #general-questions channel and be sure to read our #FAQ channel. 

